package ru.t1.chubarov.tm.api;

public interface ITaskController {

    void createTask();

    void clearTasks();

    void showTasks();

    void showTasktByIndex();

    void showTasktById();

    void updateTaskByIndex();

    void updateTaskById();

    void removeTaskByIndex();

    void removeTaskById();

    void changeTaskStatusByIndex();

    void changeTaskStatusById();

    void startTaskByIndex();

    void startTaskById();

    void completeTaskByIndex();

    void completeTaskById();
}
